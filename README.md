# Curium One 

The Curium Sat project is about the development of an open-source amateur radio experimentation CubeSat, using primarily commercial-of-the-shelf components. The Satellite has been in development from July 2023 onwards by a group formed by students from the Technical University of Berlin and radio amateurs at LibreSpace. Planetary Transportation Systems GmbH funds the launch and contributes to the open-source development. A launch opportunity with Ariane 6 is used to validate the design. 

The integrated satellite befor final integration after antenna trimming experiments:

![image](6D044306-8CF4-49CC-A9A3-5AB7F47E68FB_1_105_c.jpeg){width=30%}


# Mission Goals

## Open-Source development
The project shall contribute through open-source hard and software to satellite and amateur radio communities.
Most components of the Curium as well as their software are open-sourced, including:
- **On Board Computer (OBC)**: Managing operations in space
- **Electrical Power System (EPS)**: Handles power generation, storage and distribution
- **Battery System (BAT)**: Structural and electrical 4s2p Li-ION battery 
- **Attitude determination and control system (ADCS)**: Allows determination and control of the satelite's attitude
- **Communication System (SatNOGS-COMMS)**: [Libre Space SatNOGS-COMMS][(https://gitlab.com/librespacefoundation/satnogs-comms](https://docs.google.com/document/d/1QOcbxB9N_Mb0yUyeSQQ9H0zeVXtukuSRbpYqxWod0XE/edit#heading%253Dh.44ndaxshcuuf)) open-source UHF and S-Band capable Communication subsystem with compatability to the Satnogs network 

The open-source availability allows other amateur radio and space enthusiasts to learn and reuse the designs for their own goals! The repositories displayed here are regularly updated mirrors of private development repositories.

## Radio Amateur Experiments & Education
The Satellite mission will provide a **store and forward functionality** for radio amateurs based on the CCSDS protocol layer. Many satellites in the amateur radio spectrum previously used the [Cubesat Space Protocol](https://github.com/libcsp/libcsp). Whilst this is good for many smaller missions and still actively maintained some missions could benefit of a move towards higher level protocols like [CCSDS Space Packet Protokol](https://public.ccsds.org/Pubs/133x0b2e1.pdf) for compatibility to ground station networks and for the potential to re-use software. As CCSDS has not seen widely adapted witthin the amateur radio community this can serve as a learning ground for radio amateurs. A public page will show instructions on usage and provide documentation of the system. Furthermore a dashboard is planned that shows the number of interactions and shall enhance public visibility of the mission.
With the open-sourced hard and software, students and radio amateurs are actively organising and participating in the development of the Satellite serving various educational purposes. To support this, regular community events / presentations are performed locally.

## In-Orbit demonstration of SatNOGS-COMMS
As a secondary mission goal the in-orbit demonstration of SatNOGS-COMMS allows other entities of the Amateur Radio and Open-Source community to draw on the generated data. The SatNOGS-COMMS is a state-of-the-art open-source dual-band radio transceiver designed specifically for Telemetry and Telecommand (TMTC) operations within the S-band and UHF frequencies. The software-configurable radio module is optimized to provide robust communication channels for satellite operations. It supports in-flight reconfiguration capabilities including adjustments to carrier and intermediate frequencies, bitrate, modulation options, and channel-filter bandwidth allowing for many experimental and educational setups. SatNOGS-CPMMS achieved TRL8 in 2023 and will fly its first time in UHF only configuration on Curium.



# Technical Specifications

### Structure:
- Size: 12U or 6U CubeSat
- Bus weight: ~5.6 Kg
- Material: Aluminium 7075, 5-axis milled

### Electrical Power System (EPS)
- Solar cells: Monocrystalline Si cells (Anysolar Ixsolar based)
- Power budget: depending on orbit, 2-5 W average for payload, higher burst loads >50W possible
- Energy storage: ~100Wh Lithium-Ion LG MJ1 cells 4S2P pack with temperature measurement and thermal insulation

Battery package made of FR-4 material:

![battery image](image.png){width=25%}

Solar panel illumiated by the sun:

![solar panels image](9CB551DD-F95E-4085-9282-AA8B4F420841_1_105_c.jpeg){width=25%}

EPS structure: 

![eps structure](image-2.png){width=35%}

### Attitude Determination & Control System (ADCS)
- Magnetorquer inside solar panels 
- Possibility to integratereaction wheels

### Communications Subsystem (Satnogs-Comm)
- Utilization of Satnogs-Comms Board: [link](https://libre.space/projects/satnogs-comms/)
- Amateur radio bands for uplink and downlink, with a data transfer rate of up to 50 kbps (UHF) and possible use of S-Band 
- AT86RF215 dual-band transceiver with I/Q Radio capabilities, Half-duplex operation
- Frequency step size: 100Hz in UHF
- Receiver Noise Figure: 1.5dB
- Framing encapsulation: CCSDS
- Data rates: up to 50kbps
- GMSK/GFSK,BPSK,QPSK modulation possible
- Antenna: four monopole antennas with radiation patterns to achieve quasi-isotropic radiation properties in turnstile configuration

Satnogs Comms FM after reflow soldering:

![satnogs image](image-1.png){width=25%}

### On-Board Computer (OBC)
- CPU: STM32H7 based as [rad testing](https://www.opensourcesatellite.org/microprocessor-radiation-test-results-stm32/) indicates good long-term performance
- Redundant 32M-Bit Serial Flash Memory
OBC section of Main PCB:

![OBC image](5334E71B-FF01-4A35-A6A1-84F1A2332E8C_1_105_c.jpeg){width=25%}

### Launch & Lifetime
- Launch vehicle compatability: Designed along [Ariane 6 user's manual](https://www.arianespace.com/wp-content/uploads/2021/03/Mua-6_Issue-2_Revision-0_March-2021.pdf)
- Lifetime: Designed for >3 months. Hopes are high that the mission can be supported a lot longer!